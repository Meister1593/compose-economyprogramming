import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import enums.ActivityType
import enums.CommonChoose
import enums.Pages
import enums.Sex

@Composable
fun PagePanel(
    modifier: Modifier,
    currentPage: MutableState<Pages>
) {
    val sex: MutableState<CommonChoose> = remember { mutableStateOf(Sex.MALE) }
    val weight: MutableState<Double> = remember { mutableStateOf(80.0) }
    val height: MutableState<Double> = remember { mutableStateOf(180.0) }
    val age: MutableState<Int> = remember { mutableStateOf(30) }
    val activity: MutableState<CommonChoose> = remember { mutableStateOf(ActivityType.NONE) }
    val fatPerc: MutableState<Int> = remember { mutableStateOf(25) }
    Box(
        modifier.border(color = Color.Gray, width = 1.dp)
            .verticalScroll(rememberScrollState())
    ) {
        when (currentPage.value) {
            Pages.TITLE ->
                SelectionContainer { TitlePage() }
            Pages.INPUT -> InputPage(sex, weight, height, age, activity, fatPerc, modifier)
            Pages.MIFLIN_SANSHEORA ->
                SelectionContainer {
                    MiflinSansheoraPage(sex, weight, height, age, activity)
                }
            Pages.VOZ_CALORIES ->
                SelectionContainer {
                    VOZCaloriesPage(weight, activity, age, sex)
                }
            Pages.HARRY_BENEDICT ->
                SelectionContainer {
                    HarryBenedictPage(sex, weight, height, age, activity)
                }
            Pages.CATCH_MAKARDLOV ->
                SelectionContainer {
                    CatchMakardlovPage(weight, fatPerc, activity)
                }
            Pages.TOMM_VENUTA ->
                SelectionContainer {
                    TommVenutaPage(sex, weight, height, age, activity)
                }
            Pages.CREDITS -> CreditsPage()
        }
    }
}