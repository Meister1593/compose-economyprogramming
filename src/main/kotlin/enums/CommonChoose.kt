package enums

interface CommonChoose {
    val message: String
    val factor: Double?

    fun enumValues(): Array<CommonChoose>
}

enum class ActivityType(override val message: String, override val factor: Double) : CommonChoose {
    NONE("Пассивный, малоподвижный образ жизни, абсолютное отсутствие спорта (A)", 1.2),
    LOW("Малая активность, изредка упражнения, ходьба (A)", 1.37),
    MEDIUM("Малая активность, изредка упражнения, ходьба (A)", 1.55),
    HIGH("Высокая активность, ежедневные занятия спортом (A)", 1.7),
    EXTREME("Экстремальная подвижность, тренировки с весом (A)", 1.9);

    override fun enumValues() = values() as Array<CommonChoose>
}