package enums

enum class Pages {
    TITLE,
    INPUT,
    MIFLIN_SANSHEORA,
    VOZ_CALORIES,
    HARRY_BENEDICT,
    CATCH_MAKARDLOV,
    TOMM_VENUTA,
    CREDITS
}