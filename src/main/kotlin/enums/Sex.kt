package enums

enum class Sex(override val message: String, override val factor: Double?) : CommonChoose {
    MALE("Пол: Мужчина", null),
    FEMALE("Пол: Женщина", null);

    override fun enumValues() = values() as Array<CommonChoose>
}