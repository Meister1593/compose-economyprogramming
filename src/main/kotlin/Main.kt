import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import androidx.compose.ui.window.rememberWindowState
import enums.Pages

@Composable
@Preview
fun App() {
    MaterialTheme {
        val currentPage = remember { mutableStateOf(Pages.TITLE) }
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.border(color = Color.Gray, width = 1.dp)
        ) {
            Row {
                PageSwitcherPanel(
                    Modifier.weight(0.25f),
                    currentPage
                )
                PagePanel(Modifier.weight(1f), currentPage)
            }
        }
    }
}

fun main() = application {
    val state = rememberWindowState()
    state.size = DpSize(1200.dp, 800.dp)
    Window(
        state = state,
        onCloseRequest = ::exitApplication,
        title = "Расчёт суточной нормы калорий",
        icon = painterResource("salad.png")
    ) {
        App()
    }
}
