import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import enums.CommonChoose
import enums.Sex
import formulas.TommVenutaFormula

@Composable
fun TommVenutaPage(
    sex: MutableState<CommonChoose>,
    weight: MutableState<Double>,
    height: MutableState<Double>,
    age: MutableState<Int>,
    activity: MutableState<CommonChoose>
) {
    Column {
        Text(
            text = "Формула Томм-Венута",
            textAlign = TextAlign.Center,
            fontFamily = FontFamily.Serif,
            fontSize = 32.sp,
            modifier = Modifier.padding(8.dp)
        )
        Text(
            """
                Том Венуто -  спортсмен, пропагандист здорового образа жизни, бодибилдер. 
                Он активно занимался развенчиванием распространенных мифов. 
                В один прекрасный момент была разработана простейшая формула суточной калорийности. 
                Для нее не требуется узнавать процент жировой ткани.
                
                Расчёт по Тома Венуто:
                
                Для мужчин: 
                 - (66 + (13.7 × вес) + (5 × рост в см) – (6.8 × возраст)) × A
                Для женщин:
                 - (655 + (9.6 × вес) + (1.8 × рост в см) – (4.7 × возраст)) × A
                
                Венуто рекомендует полученное значение увеличивать на коэффициент активности, используемый в предыдущих расчетах. 
                Все мы разные. При интенсивных занятиях спортом требуется пропорциональное увеличение суточной калорийности. 
                Иначе могут возникнуть проблемы со здоровьем. Появится слабость, усталость, мышцы не смогут восстановиться.
    """.trimIndent(),
            fontFamily = FontFamily.Serif,
            modifier = Modifier.padding(8.dp)
        )
        Text(
            "Результат",
            fontSize = 24.sp,
            fontFamily = FontFamily.Serif,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.padding(8.dp)
        )
        val formula = TommVenutaFormula(
            weight.value,
            height.value,
            activity.value.factor ?: 1.0,
            age.value,
            sex.value as Sex
        )
        Text(
            "${formula.calcCalories().toInt()} ккал",
            fontFamily = FontFamily.Serif,
            fontStyle = FontStyle.Italic,
            fontSize = 18.sp,
            modifier = Modifier.padding(8.dp)
        )
    }
}