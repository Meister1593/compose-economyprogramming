import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp

@Composable
fun TitlePage() {
    Image(
        painter = painterResource("title.png"),
        contentDescription = "Титульное изображение",
        Modifier.fillMaxSize().padding(8.dp)
    )
}