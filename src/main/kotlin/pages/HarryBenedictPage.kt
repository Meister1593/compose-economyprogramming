import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import enums.CommonChoose
import enums.Sex
import formulas.HarryBenedictFormula

@Composable
fun HarryBenedictPage(
    sex: MutableState<CommonChoose>,
    weight: MutableState<Double>,
    height: MutableState<Double>,
    age: MutableState<Int>,
    activity: MutableState<CommonChoose>
) {
    Column {
        Text(
            text = "Формула Харриса-Бенедикта",
            textAlign = TextAlign.Center,
            fontFamily = FontFamily.Serif,
            fontSize = 32.sp,
            modifier = Modifier.padding(8.dp)
        )
        Text(
            """
        В начале нового тысячелетия активно применялись две формулы. Один метод изобрели ученые Маффин и Джеор.
        Другой расчет придумал Харрис-Бенедикт. Эта формула пользуется огромной популярностью.
        Оба с методом используются во многих калькуляторах онлайн.
        
        Расчёт калорий для женщин:
         - (655.1 + (9.563 × вес) + (1.85 × рост) — (4.676 × возраст)) × A.
        
        Расчёт калорий для мужчин:
         - (66,5 + (13,75 × вес) + (5,003 × рост) – (6,775 × возраст)) × A.
    """.trimIndent(),
            fontFamily = FontFamily.Serif,
            modifier = Modifier.padding(8.dp)
        )
        Text(
            "Результат",
            fontSize = 24.sp,
            fontFamily = FontFamily.Serif,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.padding(8.dp)
        )
        val formula =
            HarryBenedictFormula(weight.value, height.value, activity.value.factor ?: 1.0, age.value, sex.value as Sex)
        Text(
            "${formula.calcCalories().toInt()} ккал",
            fontFamily = FontFamily.Serif,
            fontStyle = FontStyle.Italic,
            fontSize = 18.sp,
            modifier = Modifier.padding(8.dp)
        )
    }
}