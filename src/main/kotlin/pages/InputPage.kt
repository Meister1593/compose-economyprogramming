import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import components.DropdownMenuButton
import enums.CommonChoose

@Composable
fun InputPage(
    sex: MutableState<CommonChoose>,
    weight: MutableState<Double>,
    height: MutableState<Double>,
    age: MutableState<Int>,
    activity: MutableState<CommonChoose>,
    fatPerc: MutableState<Int>,
    modifier: Modifier
) {
    Column(modifier.padding(48.dp)) {
        SelectionContainer {
            Column {
                Text(
                    "Ввод данных",
                    modifier = Modifier.padding(8.dp),
                    fontSize = 30.sp,
                    fontWeight = FontWeight.Bold,
                    fontFamily = FontFamily.Serif
                )
                Text(
                    """
                Для вычисления суточной нормы калорий необходимо ввести физические данные человека
            """.trimIndent(),
                    modifier = Modifier.padding(8.dp),
                    fontSize = 16.sp,
                    fontFamily = FontFamily.Serif
                )
            }
        }
        DropdownMenuButton(sex)
        DoubleFieldWithState("Вес человека (в кг)", weight)
        DoubleFieldWithState("Рост человека (в см)", height)
        IntFieldWithState("Возраст человека (лет)", age)
        DropdownMenuButton(activity)
        IntFieldWithState("%жира в теле", fatPerc)
    }
}