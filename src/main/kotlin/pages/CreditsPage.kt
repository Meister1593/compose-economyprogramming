
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import components.ClickableUrlText

@Composable
fun CreditsPage() {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxSize().padding(32.dp)
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.fillMaxSize().padding(8.dp)
        ) {
            SelectionContainer {
                Text(
                    text = """
                Список использованных источников
                (нажать для открытия)
            """.trimIndent(),
                    textAlign = TextAlign.Center,
                    fontFamily = FontFamily.Serif,
                    fontSize = 32.sp
                )
            }
            ClickableUrlText(
                text = "Формулы расчёта для похудения (внешний источник)",
                url = "https://fordiets.ru/formuly-rascheta-kalorij-dlya-poxudeniya.html"
            )
            ClickableUrlText(
                text = "Полезная математика: как считать калории? (внешний источник)",
                url = "https://www.championat.com/lifestyle/article-3472531-schitaem-kalorii-kak-vyschitat-svojo-kbzhu-fitnes-i-pp.html"
            )
            ClickableUrlText(
                text = "Как рассчитать суточную калорийность (внешний источник)",
                url = "https://primekraft.ru/articles/kak-rasschitat-sutochnuyu-kalorijnost-ratsiona-formulyi-rascheta"
            )
        }
    }
}