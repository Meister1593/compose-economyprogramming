import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import enums.CommonChoose
import enums.Sex
import formulas.VOZCaloriesFormula

@Composable
fun VOZCaloriesPage(
    weight: MutableState<Double>,
    activity: MutableState<CommonChoose>,
    age: MutableState<Int>,
    sex: MutableState<CommonChoose>
) {
    Column {
        Text(
            text = "Формула ВОЗ",
            textAlign = TextAlign.Center,
            fontFamily = FontFamily.Serif,
            fontSize = 32.sp,
            modifier = Modifier.padding(8.dp)
        )
        Text(
            """
                Всемирная организация здравоохранения озабочена стройностью населения. 
                На планете растет процент людей с ожирением. ВОЗ предлагает свои методы расчета КБЖУ.
                
                Формулы для женщин:
                 - 18-30 лет: ((0,062 × вес (кг) + 2,036) × 240) × A
                 - 31-60 лет: ((0,034 × вес (кг) + 3,538) × 240) × A
                 - возраст 60+: ((0,038 × вес (кг) + 2,755) × 240) × A
                Формулы для мужчин:
                 - 18 – 30 лет: ((0,063 × вес (кг) + 2,896) × 240) × A
                 - 31-60 лет: ((0,0484 × вес (кг) + 3,653) × 240) × A
                 - возраст 60+: ((0,0491 × вес (кг) + 2,459) × 240) × A
    """.trimIndent(),
            fontFamily = FontFamily.Serif,
            modifier = Modifier.padding(8.dp)
        )
        Text(
            "Результат",
            fontSize = 24.sp,
            fontFamily = FontFamily.Serif,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.padding(8.dp)
        )
        val formula = VOZCaloriesFormula(weight.value, activity.value.factor ?: 1.0, age.value, sex.value as Sex)
        Text(
            "${formula.calcCalories().toInt()} ккал",
            fontFamily = FontFamily.Serif,
            fontStyle = FontStyle.Italic,
            fontSize = 18.sp,
            modifier = Modifier.padding(8.dp)
        )
    }
}