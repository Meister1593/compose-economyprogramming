import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import enums.CommonChoose
import enums.Sex
import formulas.MiflinSansheoraFormula

@Composable
fun MiflinSansheoraPage(
    sex: MutableState<CommonChoose>,
    weight: MutableState<Double>,
    height: MutableState<Double>,
    age: MutableState<Int>,
    activity: MutableState<CommonChoose>
) {
    Column {
        Text(
            text = "Формула Миффлина-Сан Жеора",
            textAlign = TextAlign.Center,
            fontFamily = FontFamily.Serif,
            fontSize = 32.sp,
            modifier = Modifier.padding(8.dp)
        )
        Text(
            text = """
        Самая популярная формула для расчета калорий - Миффлина-Сан Жеора. 
        Результат работы группы американских и японских учёных. Он появился относительно недавно -
        в 90х годах прошлого тысячелетия. Основные разработчики дали названию метода свои фамилии.
        
        Расчёт калорий для мужчин:
        """.trimIndent(),
            fontFamily = FontFamily.Serif,
            modifier = Modifier.padding(8.dp)
        )
        Text(
            text = " - (9.99× вес (килограммы) + 6.25 × рост (в см) – 4.92 × возраст (лет) + 5) × A",
            fontFamily = FontFamily.Serif,
            fontStyle = FontStyle.Italic,
            modifier = Modifier.padding(8.dp)
        )
        Text(
            text = "Расчёт калорий для женщин:",
            fontFamily = FontFamily.Serif,
            modifier = Modifier.padding(8.dp)
        )
        Text(
            text = " - (9.99 × вес (килограммы) + 6.25 × рост (в см) – 4.92 × возраст (лет) – 161) × A",
            fontFamily = FontFamily.Serif,
            fontStyle = FontStyle.Italic,
            modifier = Modifier.padding(8.dp)
        )
        Text(
            "Где A — коэффициент активности. Согласно исследованиям Миффилина и Сан Жеора, люди по энергозатратности делятся на пять основных групп.",
            fontFamily = FontFamily.Serif,
            modifier = Modifier.padding(8.dp)
        )
        Text(
            "Результат",
            fontSize = 24.sp,
            fontFamily = FontFamily.Serif,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.padding(8.dp)
        )
        val formula = MiflinSansheoraFormula(
            weight.value,
            height.value,
            age.value,
            sex.value as Sex,
            activity.value.factor ?: 1.0
        )
        Text(
            "${formula.calcCalories().toInt()} ккал",
            fontFamily = FontFamily.Serif,
            fontStyle = FontStyle.Italic,
            fontSize = 18.sp,
            modifier = Modifier.padding(8.dp)
        )
    }
}