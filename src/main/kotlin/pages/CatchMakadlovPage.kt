import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import enums.CommonChoose
import formulas.CatchMakardlovFormula

@Composable
fun CatchMakardlovPage(
    weight: MutableState<Double>,
    fatPerc: MutableState<Int>,
    activity: MutableState<CommonChoose>
) {
    Column {
        Text(
            text = "Формула Кэтча-МакАрдла",
            textAlign = TextAlign.Center,
            fontFamily = FontFamily.Serif,
            fontSize = 32.sp,
            modifier = Modifier.padding(8.dp)
        )
        Text(
            """
        В отличие от выше описанных формул для определения нормы калорий, Кэтч Макардл при расчетах учитывает процент жира.
        Это один из самых точных способов высчитать дневную калорийность.
        Единственный недостаток — необходимо знать количество жировой ткани в организме.
        Это можно определить с помощью специального биоимпедансного анализа.
        
        Прибор для определения процента жира есть в клиниках по похудению и продвинутых спортивных клубах.
        Приблизительное значение показателя помогут узнать специальные «умные» весы.
        
        Расчет нормы калорий в день производится по формуле:
         - BMR = (370 + (21.6 × LBM)) × A
        
        Где A — коэффициент активности.
        
        LBM рассчитываем по формуле:
         - LBM = вес (кг) × (100 — %жира)/100
    """.trimIndent(),
            fontFamily = FontFamily.Serif,
            modifier = Modifier.padding(8.dp)
        )
        Text(
            "Результат",
            fontSize = 24.sp,
            fontFamily = FontFamily.Serif,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.padding(8.dp)
        )
        val formula = CatchMakardlovFormula(weight.value, fatPerc.value, activity.value.factor ?: 1.0)
        Text(
            "${formula.calcCalories().toInt()} ккал",
            fontFamily = FontFamily.Serif,
            fontStyle = FontStyle.Italic,
            fontSize = 18.sp,
            modifier = Modifier.padding(8.dp)
        )
    }
}