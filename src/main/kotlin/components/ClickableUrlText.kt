package components

import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.withStyle

@Composable
fun ClickableUrlText(text: String, url: String) {
    val page = buildAnnotatedString {
        pushStringAnnotation(
            tag = "URL",
            annotation = url
        )
        withStyle(style = SpanStyle(color = MaterialTheme.colors.primary)) {
            append(text)
        }
        pop()
    }
    val uriHandler = LocalUriHandler.current
    ClickableText(
        text = page,
        onClick = {
            page.getStringAnnotations("URL", it, it)
                .firstOrNull()?.let { stringAnnotation ->
                    val runtime = Runtime.getRuntime();
                    if (System.getProperty("os.name").lowercase().contains("linux")
                        && runtime.exec(arrayOf("which", "xdg-open")).inputStream.read() != -1
                    ) {
                        runtime.exec(arrayOf("xdg-open", stringAnnotation.item))
                        return@ClickableText
                    }
                    uriHandler.openUri(stringAnnotation.item)
                }
        }
    )
}