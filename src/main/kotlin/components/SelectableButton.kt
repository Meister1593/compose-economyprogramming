package components

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color

@Composable
fun SelectableButton(selected: Boolean, text: String, onClick: () -> Unit) {
    Row {
        var color = ButtonDefaults.buttonColors()
        if (selected) {
            color = ButtonDefaults.buttonColors(backgroundColor = Color.Green)
        }
        Button(
            modifier = Modifier.fillMaxWidth(),
            colors = color,
            onClick = onClick
        ) {
            Text(text)
        }
    }
}