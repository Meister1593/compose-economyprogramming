import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp

@Composable
fun DoubleFieldWithState(text: String,
                         state: MutableState<Double>,
                         modifier: Modifier = Modifier.padding(8.dp)) {
    TextField(
        label = { Text(text, fontFamily = FontFamily.Serif) },
        value = state.value.toString(),
        onValueChange = {
            val value = it.toDoubleOrNull() ?: return@TextField
            state.value = value
        },
        modifier = modifier
    )
}