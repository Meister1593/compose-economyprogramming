package components

import androidx.compose.foundation.layout.Column
import androidx.compose.material.Button
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import enums.CommonChoose

@Composable
fun DropdownMenuButton(enumValue: MutableState<CommonChoose>) {
    val isExpanded = remember { mutableStateOf(false) }
    Column {
        Button(onClick = { isExpanded.value = true }) {
            Text(enumValue.value.message)
        }
        DropdownMenu(
            expanded = isExpanded.value,
            onDismissRequest = { isExpanded.value = false }
        ) {
            enumValue.value.enumValues().forEach {
                DropdownMenuItem(
                    onClick = {
                        isExpanded.value = false
                        enumValue.value = it
                    },
                ) {
                    Text(it.message)
                }
            }
        }
    }
}
