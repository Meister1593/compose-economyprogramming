import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import components.SelectableButton
import enums.Pages

@Composable
fun PageSwitcherPanel(
    modifier: Modifier, currentPage: MutableState<Pages>
) {
    Column(
        modifier = modifier.fillMaxHeight()
            .defaultMinSize(minWidth = 16.dp, minHeight = 16.dp)
            .background(MaterialTheme.colors.surface)
            .padding(1.dp)
            .background(Color.White)
            .border(color = Color.Gray, width = 1.dp)
            .padding(start = 16.dp, end = 16.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.End
    ) {
        var titlePageButtonSelectionHidden by remember { mutableStateOf(true) }
        var inputPageButtonSelectionHidden by remember { mutableStateOf(false) }
        var miflinSansheoraPageButtonSelectionHidden by remember { mutableStateOf(false) }
        var vozPageButtonSelectionHidden by remember { mutableStateOf(false) }
        var harryBenedictPageButtonSelectionHidden by remember { mutableStateOf(false) }
        var catchMakardlovPageButtonSelectionHidden by remember { mutableStateOf(false) }
        var tommVenutaFormulaPageButtonSelectionHidden by remember { mutableStateOf(false) }
        var creditsPageButtonSelectionHidden by remember { mutableStateOf(false) }
        fun resetSelections() {
            titlePageButtonSelectionHidden = false
            inputPageButtonSelectionHidden = false
            miflinSansheoraPageButtonSelectionHidden = false
            vozPageButtonSelectionHidden = false
            harryBenedictPageButtonSelectionHidden = false
            catchMakardlovPageButtonSelectionHidden = false
            tommVenutaFormulaPageButtonSelectionHidden = false
            creditsPageButtonSelectionHidden = false
        }
        SelectableButton(
            selected = titlePageButtonSelectionHidden,
            onClick = {
                resetSelections()
                titlePageButtonSelectionHidden = true
                currentPage.value = Pages.TITLE
            },
            text = "Титульная страница"
        )
        SelectableButton(
            selected = inputPageButtonSelectionHidden,
            onClick = {
                resetSelections()
                inputPageButtonSelectionHidden = true
                currentPage.value = Pages.INPUT
            },
            text = "Страница ввода данных"
        )
        SelectableButton(
            selected = miflinSansheoraPageButtonSelectionHidden,
            onClick = {
                resetSelections()
                miflinSansheoraPageButtonSelectionHidden = true
                currentPage.value = Pages.MIFLIN_SANSHEORA
            },
            text = "Страница расчёт по формуле Миффлина-Сан Жеора"
        )
        SelectableButton(
            selected = vozPageButtonSelectionHidden,
            onClick = {
                resetSelections()
                vozPageButtonSelectionHidden = true
                currentPage.value = Pages.VOZ_CALORIES
            },
            text = "Страница расчёт по формуле калорийности ВОЗ"
        )
        SelectableButton(
            selected = harryBenedictPageButtonSelectionHidden,
            onClick = {
                resetSelections()
                harryBenedictPageButtonSelectionHidden = true
                currentPage.value = Pages.HARRY_BENEDICT
            },
            text = "Страница расчёт по формуле Харриса-Бенедикта"
        )
        SelectableButton(
            selected = catchMakardlovPageButtonSelectionHidden,
            onClick = {
                resetSelections()
                catchMakardlovPageButtonSelectionHidden = true
                currentPage.value = Pages.CATCH_MAKARDLOV
            },
            text = "Страница расчёт по формуле Кэтча-МакАрдла"
        )
        SelectableButton(
            selected = tommVenutaFormulaPageButtonSelectionHidden,
            onClick = {
                resetSelections()
                tommVenutaFormulaPageButtonSelectionHidden = true
                currentPage.value = Pages.TOMM_VENUTA
            },
            text = "Страница расчёт по формуле Тома Венуто"
        )
        SelectableButton(
            selected = creditsPageButtonSelectionHidden,
            onClick = {
                resetSelections()
                creditsPageButtonSelectionHidden = true
                currentPage.value = Pages.CREDITS
            },
            text = "Список использованных источников"
        )
    }
}