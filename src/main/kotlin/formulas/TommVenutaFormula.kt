package formulas

import enums.Sex

class TommVenutaFormula(val weight: Double, val height: Double, val A: Double, val age: Int, val sex: Sex) {
    fun calcCalories(): Double {
        return when (sex) {
            Sex.MALE -> (66 + (13.7 * weight) + (5 * height) - (6.8 * age)) * A
            Sex.FEMALE -> (655 + (9.6 * weight) + (1.8 * height) - (4.7 * age)) * A
        }
    }
}