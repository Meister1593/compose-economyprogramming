package formulas

import enums.Sex

class VOZCaloriesFormula(val weight: Double, val KFA: Double, val age: Int, val sex: Sex) {
    fun calcCalories(): Double {
        return when (sex) {
            Sex.MALE -> when (age) {
                in 18..30 -> (0.063 * weight + 2.896) * 240 * KFA
                in 31..60 -> (0.0484 * weight + 3.653) * 240 * KFA
                in 61..Int.MAX_VALUE -> (0.0491 * weight + 2.459) * 240 * KFA
                else -> {
                    0.0
                }
            }
            Sex.FEMALE -> when (age) {
                in 18..30 -> (0.062 * weight + 2.036) * 240 * KFA
                in 31..60 -> (0.034 * weight + 3.538) * 240 * KFA
                in 61..Int.MAX_VALUE -> (0.038 * weight + 2.755) * 240 * KFA
                else -> {
                    0.0
                }
            }
        }
    }
}