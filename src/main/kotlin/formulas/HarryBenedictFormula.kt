package formulas

import enums.Sex

class HarryBenedictFormula(val weight: Double, val height: Double, val A: Double, val age: Int, val sex: Sex) {
    fun calcCalories(): Double {
        return when (sex) {
            Sex.MALE -> (66.5 + (13.75 * weight) + (5.003 * height) - (6.775 * age)) * A
            Sex.FEMALE -> (655.1 + (9.563 * weight) + (1.85 * height) - (4.676 * age)) * A
        }
    }
}