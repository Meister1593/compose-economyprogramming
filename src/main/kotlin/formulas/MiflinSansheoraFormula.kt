package formulas

import enums.Sex

class MiflinSansheoraFormula(val weight: Double, val height: Double, val age: Int, val sex: Sex, val A: Double) {
    fun calcCalories(): Double {
        return when (sex) {
            Sex.MALE -> (9.99 * weight + 6.25 * height - 4.92 * age + 5) * A
            Sex.FEMALE -> (9.99 * weight + 6.25 * height - 4.92 * age - 161) * A
        }
    }
}