package formulas

class CatchMakardlovFormula(val weight: Double, val fatPerc: Int, val A: Double) {
    fun calcCalories(): Double {
        val LBM = weight * (100 - fatPerc) / 100
        return (370 + (21.6 * LBM)) * A
    }
}